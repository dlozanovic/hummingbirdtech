package com.dejan.lozanovic.hummingbirdtech.repository;

import com.dejan.lozanovic.hummingbirdtech.domain.dao.Harvest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface HarvestRepository extends CrudRepository<Harvest, UUID> {

}
