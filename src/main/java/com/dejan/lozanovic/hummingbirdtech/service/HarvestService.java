package com.dejan.lozanovic.hummingbirdtech.service;

import com.dejan.lozanovic.hummingbirdtech.domain.dao.Harvest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.DaysHarvest;
import com.dejan.lozanovic.hummingbirdtech.repository.HarvestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.Temporal;
import java.util.Optional;
import java.util.UUID;

@Service
public class HarvestService {

    private HarvestRepository repository;

    public HarvestService(@Autowired HarvestRepository repository) {
        this.repository = repository;
    }

    public Optional<DaysHarvest> getNumberOfDaysUntilHarvest(UUID id) {
        return repository.findById(id).map(h -> {
                    Date harvestDate = h.getHarvestDate();
                    long numberOfDays = daysUntilHarvest(LocalDate.now().atStartOfDay(), harvestDate.toLocalDate().atStartOfDay());
                    return new DaysHarvest(numberOfDays);
                }
        );
    }

    public Harvest saveNew(Harvest record) {
        record.setId(UUID.randomUUID());
        return repository.save(record);
    }

    public Optional<Harvest> updateHarvest(Harvest record) {
        if (repository.findById(record.getId()).isPresent()) {
            return Optional.of(repository.save(record));
        } else {
            return Optional.empty();
        }
    }


    public Long daysUntilHarvest(Temporal now, Temporal date) {
        return Math.max(Duration.between(now, date).toDays(), 0);
    }
}
