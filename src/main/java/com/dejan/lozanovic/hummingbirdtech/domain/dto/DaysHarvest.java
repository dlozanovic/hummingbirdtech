package com.dejan.lozanovic.hummingbirdtech.domain.dto;


import java.util.Objects;

public class DaysHarvest {
    private long daysUntilHarvest;

    public DaysHarvest(long daysUntilHarvest) {
        this.daysUntilHarvest = daysUntilHarvest;
    }

    public DaysHarvest() {
    }

    public long getDaysUntilHarvest() {
        return daysUntilHarvest;
    }

    public void setDaysUntilHarvest(long daysUntilHarvest) {
        this.daysUntilHarvest = daysUntilHarvest;
    }

    @Override
    public String toString() {
        return "DaysHarvest{" +
                "daysUntilHarvest=" + daysUntilHarvest +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DaysHarvest that = (DaysHarvest) o;
        return daysUntilHarvest == that.daysUntilHarvest;
    }

    @Override
    public int hashCode() {
        return Objects.hash(daysUntilHarvest);
    }
}
