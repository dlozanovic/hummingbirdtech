package com.dejan.lozanovic.hummingbirdtech.domain.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Date;
import java.util.Objects;
import java.util.UUID;


@Entity
public class Harvest {

    @Id
    @GeneratedValue
    private UUID id;
    private Date harvestDate;

    public Harvest(UUID id, Date harvestDate) {
        this.id = id;
        this.harvestDate = harvestDate;
    }

    public Harvest() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getHarvestDate() {
        return harvestDate;
    }

    public void setHarvestDate(Date harvestDate) {
        this.harvestDate = harvestDate;
    }

    @Override
    public String toString() {
        return "Harvest{" +
                "id=" + id +
                ", harvestDate=" + harvestDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Harvest harvest = (Harvest) o;
        return Objects.equals(id, harvest.id) &&
                Objects.equals(harvestDate, harvest.harvestDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, harvestDate);
    }
}
