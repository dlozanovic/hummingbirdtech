package com.dejan.lozanovic.hummingbirdtech.domain.dto;

import java.util.Objects;
import java.util.UUID;

public class NewHarvestResponse {

    private UUID id;

    public NewHarvestResponse(UUID id) {
        this.id = id;
    }

    public NewHarvestResponse() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "NewHarvestResponse{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewHarvestResponse that = (NewHarvestResponse) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
