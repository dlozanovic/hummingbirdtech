package com.dejan.lozanovic.hummingbirdtech.controller;

import com.dejan.lozanovic.hummingbirdtech.domain.dao.Harvest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.DaysHarvest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.NewHarvestRequest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.NewHarvestResponse;
import com.dejan.lozanovic.hummingbirdtech.service.HarvestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/harvests")
public class HarvestController {

    @Autowired
    private HarvestService service;

    public HarvestController() {
    }

    public HarvestController(HarvestService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public DaysHarvest getDaysUntilHarvest(@PathVariable UUID id) {
        Optional<DaysHarvest> numberOfDaysUntilHarvest = service.getNumberOfDaysUntilHarvest(id);
        return numberOfDaysUntilHarvest.orElseThrow(IdNotFoundException::new);
    }

    @PostMapping
    public ResponseEntity<?> createNewHarvest(@Valid @RequestBody NewHarvestRequest request) {
        Harvest harvest = new Harvest();
        harvest.setHarvestDate(Date.valueOf(request.getHarvestDate()));

        Harvest saved = service.saveNew(harvest);
        return  new ResponseEntity<>(new NewHarvestResponse(saved.getId()), HttpStatus.CREATED) ;
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateHarvest(@Valid @RequestBody NewHarvestRequest request, @PathVariable UUID id) {
        Harvest harvest = new Harvest();
        harvest.setId(id);
        harvest.setHarvestDate(Date.valueOf(request.getHarvestDate()));

        if(service.updateHarvest(harvest).isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new IdNotFoundException();
        }
    }
}
