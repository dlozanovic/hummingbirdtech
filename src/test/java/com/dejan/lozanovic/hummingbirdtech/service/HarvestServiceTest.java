package com.dejan.lozanovic.hummingbirdtech.service;


import com.dejan.lozanovic.hummingbirdtech.domain.dao.Harvest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.DaysHarvest;
import com.dejan.lozanovic.hummingbirdtech.repository.HarvestRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

@RunWith(MockitoJUnitRunner.class)
public class HarvestServiceTest {

    @Mock
    HarvestRepository repository;

    HarvestService service;

    @Before
    public void setUp() {
        service = new HarvestService(repository);
    }

    @Test
    public void testDaysUntilHarvest() throws Exception {
        Instant present = Instant.now();
        Instant past = Instant.EPOCH;

        // date is in the future compared to now so it should be greater than zero
        Assertions.assertThat(service.daysUntilHarvest(past, present))
                .isGreaterThan(0);

        // date is in the past compared to now, so result should be 0
        Assertions.assertThat(service.daysUntilHarvest(present, past))
                .isEqualTo(0);

        Instant twoDaysInFuture = present.plus(2, ChronoUnit.DAYS);


        // date is in the past compared to now, so result should be 0
        Assertions.assertThat(service.daysUntilHarvest(present, twoDaysInFuture))
                .isEqualTo(2);
    }

    @Test
    public void testGetNumberOfDaysUntilHarvestHaveRecordInRepository() throws Exception {
        UUID id = UUID.randomUUID();
        Date date = Date.valueOf(LocalDate.now().plus(42, ChronoUnit.DAYS));

        Harvest record = new Harvest(id, date);

        when(repository.findById(id)).thenReturn(Optional.of(record));
        Assertions.assertThat(service.getNumberOfDaysUntilHarvest(id)).isEqualTo(Optional.of(new DaysHarvest(42)));
        verify(repository).findById(id);
    }

    @Test
    public void testGetNumberOfDaysUntilHarvestDoesNotHaveRecordInRepository() throws Exception {
        UUID id = UUID.randomUUID();

        when(repository.findById(any())).thenReturn(Optional.empty());
        Assertions.assertThat(service.getNumberOfDaysUntilHarvest(id)).isEqualTo(Optional.empty());
        verify(repository).findById(id);
    }


    @Test
    public void testSaveNew() throws Exception {
        Harvest record = new Harvest();
        record.setHarvestDate(Date.valueOf(LocalDate.now().plus(42, ChronoUnit.DAYS)));

        when(repository.save(record)).thenReturn(record);
        Assertions.assertThat(service.saveNew(record)).isEqualTo(record);
        verify(repository).save(record);
    }

    @Test
    public void testUpdateHaveRecordInRepository() throws Exception {
        UUID id = UUID.randomUUID();
        Date date = Date.valueOf(LocalDate.now().plus(42, ChronoUnit.DAYS));
        Harvest oldRecord = new Harvest(id, date);
        Harvest record = new Harvest(id, date);

        when(repository.findById(id)).thenReturn(Optional.of(oldRecord));
        when(repository.save(record)).thenReturn(record);

        Assertions.assertThat(service.updateHarvest(record)).isEqualTo(Optional.of(record));
        verify(repository).findById(id);
        verify(repository).save(record);
    }


    @Test
    public void testUpdateDoesNotHaveRecordInRepository() throws Exception {
        UUID id = UUID.randomUUID();
        Date date = Date.valueOf(LocalDate.now().plus(42, ChronoUnit.DAYS));
        Harvest record = new Harvest(id, date);

        when(repository.findById(id)).thenReturn(Optional.empty());

        Assertions.assertThat(service.updateHarvest(record)).isEqualTo(Optional.empty());
        verify(repository).findById(id);
        verify(repository,times(0)).save(any());
    }
}
