package com.dejan.lozanovic.hummingbirdtech.controller;


import com.dejan.lozanovic.hummingbirdtech.domain.dao.Harvest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.DaysHarvest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.NewHarvestRequest;
import com.dejan.lozanovic.hummingbirdtech.domain.dto.NewHarvestResponse;
import com.dejan.lozanovic.hummingbirdtech.service.HarvestService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(HarvestController.class)
public class HarvestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private HarvestService service;


    Harvest record = new Harvest(
            UUID.randomUUID(),
            Date.valueOf(LocalDate.now().plus(2, ChronoUnit.DAYS)));

    @Before
    public void setUpEnvironment() {
    }

    @Test
    public void testGetRecord() throws Exception {
        DaysHarvest response = new DaysHarvest(42);

        when(service.getNumberOfDaysUntilHarvest(record.getId())).thenReturn(Optional.of(response));

        mvc.perform(MockMvcRequestBuilders.get("/harvests/" + record.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(toJson(response)));
    }

    @Test
    public void testGetRecordNotExists() throws Exception {
        when(service.getNumberOfDaysUntilHarvest(record.getId())).thenReturn(Optional.empty());

        mvc.perform(MockMvcRequestBuilders.get("/harvests/" + record.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("id not found"));

    }

    @Test
    public void testInsertNewRecord() throws Exception {
        when(service.saveNew(any())).thenReturn(record);

        NewHarvestRequest request = new NewHarvestRequest();
        request.setHarvestDate(record.getHarvestDate().toLocalDate());

        NewHarvestResponse response = new NewHarvestResponse();
        response.setId(record.getId());

        mvc.perform(MockMvcRequestBuilders.post("/harvests")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(request)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().json(toJson(response)));

    }

    @Test
    public void testUpdateRecord() throws Exception {
        when(service.updateHarvest(any())).thenReturn(Optional.of(record));

        NewHarvestRequest request = new NewHarvestRequest();
        request.setHarvestDate(record.getHarvestDate().toLocalDate());

        mvc.perform(MockMvcRequestBuilders.put("/harvests/" + record.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(request)))
                .andExpect(status().isNoContent());

    }


    @Test
    public void testUpdateRecordNotExists() throws Exception {
        when(service.updateHarvest(any())).thenReturn(Optional.empty());

        NewHarvestRequest request = new NewHarvestRequest();
        request.setHarvestDate(record.getHarvestDate().toLocalDate());

        mvc.perform(MockMvcRequestBuilders.put("/harvests/" + record.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(request)))
                .andExpect(status().isNotFound())
                .andExpect(status().reason("id not found"));

    }


    private String toJson(Object any) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());

        String result = mapper.writeValueAsString(any);
        return result;
    }

}
