#Installation

mvn package

#Run
java -jar target/hummingbirdtech-1.0-SNAPSHOT.jar

# API Documentation

## Insert new record

Send POST request to address http://localhost:8080/harvests/

with json content like :

```json
{
  "harvestDate":"2019-07-03"
}
``` 

Date format is yyyy-MM-dd

## Update existing record

Send PUT request to http://localhost:8080/harvests/{id}

with json content like : 

```json
{
    "harvestDate":"2019-07-03"
}
``` 
on url like http://localhost:8080/harvests/2a053dba-525e-4bf5-ad08-88f163ce072c

## Get days until harvest

Send GET request to 
http://localhost:8080/harvests/{id}

example:

http://localhost:8080/harvests/2a053dba-525e-4bf5-ad08-88f163ce072c

